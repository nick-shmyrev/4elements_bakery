if (/MSIE 10/i.test(navigator.userAgent)) {
   // this is internet explorer 10
   window.alert('This website is not intended to be viewed in Internet Explorer.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)){
    // this is internet explorer 9 and 11
    window.alert('This website is not intended to be viewed in Internet Explorer.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

if (/Edge\/12./i.test(navigator.userAgent)){
   // this is Microsoft Edge
   window.alert('This website is not intended to be viewed in Edge 12.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

var btn_menu = document.getElementById("menu-toggle");
var nav = document.querySelector("header ul");

btn_menu.addEventListener("click", function(){
  nav.classList.add("show");
});
window.addEventListener("click", function(event){
  if( !event.target.matches('#menu-toggle') && nav.classList.contains("show") ){
    nav.classList.remove("show");
  }
});
